package com.kd.shapes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class CircleTest {

    Circle circle;

    @Before
    public void before(){
        circle = new Circle( 10d);
    }

    @Test
    public void testSmall(){
        Assert.assertEquals( Optional.of( circle.calculateArea() ), Optional.of( 314.1592653589793 ) );
        Assert.assertEquals( Optional.of( circle.calculatePerimeter() ), Optional.of( 62.83185307179586 ) );
        circle.resize( .5 );
        Assert.assertEquals( Optional.of( circle.calculateArea() ), Optional.of( 78.53981633974483 ) );
        Assert.assertEquals( Optional.of( circle.calculatePerimeter() ), Optional.of( 31.41592653589793 ) );
    }

}
