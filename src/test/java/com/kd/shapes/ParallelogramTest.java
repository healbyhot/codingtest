package com.kd.shapes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class ParallelogramTest {

    Parallelogram parallelogram;

    @Before
    public void before() {
        parallelogram = new Parallelogram( 4d, 6d );
    }

    @Test
    public void testParallelogram(){
        Assert.assertEquals( Optional.of( parallelogram.calculateArea() ), Optional.of( 24.0 ) );
        Assert.assertEquals( Optional.of( parallelogram.calculatePerimeter() ), Optional.of( 20.0 ) );
        parallelogram.resize( 2d );
        Assert.assertEquals( Optional.of( parallelogram.calculateArea() ), Optional.of( 96.0 ) );
        Assert.assertEquals( Optional.of( parallelogram.calculatePerimeter() ), Optional.of( 40.0 ) );
    }
}
