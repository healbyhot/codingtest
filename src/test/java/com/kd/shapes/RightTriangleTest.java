package com.kd.shapes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class RightTriangleTest {

    RightTriangle rightTriangle;

    @Before
    public void before(){
        rightTriangle = new RightTriangle( 3d, 4d );
    }

    @Test
    public void testRightTriangle(){
        Assert.assertEquals( Optional.of( rightTriangle.getHypotenuse() ), Optional.of( 5.0 ) );
        Assert.assertEquals( Optional.of( rightTriangle.calculateArea() ), Optional.of( 6.0 ) );
        Assert.assertEquals( Optional.of( rightTriangle.calculatePerimeter() ), Optional.of( 12.0 ) );
        rightTriangle.resize( 4d );
        Assert.assertEquals( Optional.of( rightTriangle.getHypotenuse() ), Optional.of( 20.0 ) );
        Assert.assertEquals( Optional.of( rightTriangle.calculateArea() ), Optional.of( 96.0 ) );
        Assert.assertEquals( Optional.of( rightTriangle.calculatePerimeter() ), Optional.of( 48.0 ) );
    }
}