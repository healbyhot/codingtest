package com.kd.shapes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class SquareTest {

    Square square;

    @Before
    public void before(){
        square = new Square( 2d );
    }

    @Test
    public void testSquare() {
        Assert.assertEquals( Optional.of( square.calculateArea() ), Optional.of( 4.0 ) );
        Assert.assertEquals( Optional.of( square.calculatePerimeter() ), Optional.of( 8.0 ) );
        square.resize( 2d );
        Assert.assertEquals( Optional.of( square.calculateArea() ), Optional.of( 16.0) );
        Assert.assertEquals( Optional.of( square.calculatePerimeter() ), Optional.of( 16.0 ) );
    }
}
