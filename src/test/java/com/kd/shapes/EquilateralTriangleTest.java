package com.kd.shapes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class EquilateralTriangleTest {

    EquilateralTriangle equilateralTriangle;

    @Before
    public void before(){
        equilateralTriangle = new EquilateralTriangle( 3d );
    }

    @Test
    public void testEquilateralTriangle(){
        Assert.assertEquals( Optional.of( equilateralTriangle.calculateArea() ), Optional.of( 3.8971143170299736 ) );
        Assert.assertEquals( Optional.of( equilateralTriangle.calculatePerimeter() ), Optional.of( 9.0 ) );
        equilateralTriangle.resize( 2d );
        Assert.assertEquals( Optional.of( equilateralTriangle.calculateArea() ), Optional.of( 15.588457268119894 ) );
        Assert.assertEquals( Optional.of( equilateralTriangle.calculatePerimeter() ), Optional.of( 18.0 ) );
    }
}
