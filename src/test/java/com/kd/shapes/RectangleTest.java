package com.kd.shapes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class RectangleTest {

    Rectangle rectangle;

    @Before
    public void before(){
        rectangle = new Rectangle( 3d, 4d );
    }

    @Test
    public void testRectangle(){
        Assert.assertEquals( Optional.of( rectangle.calculateArea() ), Optional.of( 12.0 ) );
        Assert.assertEquals( Optional.of( rectangle.calculatePerimeter() ), Optional.of( 14.0 ) );
        rectangle.resize( 2d );
        Assert.assertEquals( Optional.of( rectangle.calculateArea() ), Optional.of( 48.0 ) );
        Assert.assertEquals( Optional.of( rectangle.calculatePerimeter() ), Optional.of( 28.0 ) );
    }
}
