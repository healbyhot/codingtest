package com.kd.shapes;

public interface Shape {
    Double calculateArea();
    Double calculatePerimeter();
    void resize(Double scale);
}
