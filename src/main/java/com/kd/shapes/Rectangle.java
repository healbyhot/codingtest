package com.kd.shapes;

public class Rectangle implements Shape {

    private Double length, width;

    public Rectangle(Double length, Double width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public Double calculateArea() {
        return length * width;
    }

    @Override
    public Double calculatePerimeter() {
        return 2 * ( length + width );
    }

    @Override
    public void resize(Double scale) {
        this.length *= scale;
        this.width *= scale;
    }
}
