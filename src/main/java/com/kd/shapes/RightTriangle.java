package com.kd.shapes;

public class RightTriangle implements Shape {

    private Double a, b, hypotenuse;

    public RightTriangle(Double a, Double b) {
        this.a = a;
        this.b = b;
        this.hypotenuse = calculateHypotenuse();
    }

    public Double getHypotenuse(){
        return this.hypotenuse;
    }

    private Double calculateHypotenuse() {
        return Math.sqrt( Math.pow( this.a, 2) + Math.pow( this.b, 2 ) );
    }

    @Override
    public Double calculateArea() {
        return (a*b)/2;
    }

    @Override
    public Double calculatePerimeter() {
        return a + b + hypotenuse;
    }

    @Override
    public void resize(Double scale) {
        this.a *= scale;
        this.b *= scale;
        this.hypotenuse = calculateHypotenuse();
    }
}
