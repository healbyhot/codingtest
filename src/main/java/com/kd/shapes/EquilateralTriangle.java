package com.kd.shapes;

public class EquilateralTriangle implements Shape {

    public Double a;

    public EquilateralTriangle(Double a) {
        this.a = a;
    }

    @Override
    public Double calculateArea() {
        return ( Math.sqrt(3) / 4 ) * Math.pow( a, 2 );
    }

    @Override
    public Double calculatePerimeter() {
        return 3*a;
    }

    @Override
    public void resize(Double scale) {
        this.a *= scale;
    }
}
