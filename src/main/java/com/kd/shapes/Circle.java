package com.kd.shapes;

public class Circle implements Shape {

    private Double radius;

    public Circle(Double radius) {
        this.radius = radius;
    }

    @Override
    public Double calculateArea() {
        return Math.pow( radius, 2 ) * Math.PI;
    }

    @Override
    public Double calculatePerimeter() {
        return 2 * radius * Math.PI;
    }

    @Override
    public void resize(Double scale) {
        this.radius *= (scale);
    }
}
